<?php
session_start();
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>modification ingrédient</title>
    <link rel="stylesheet" href="pageadmin.css">
</head>
<body>
<?php
        try{

            $host = 'localhost';
            $db = 'fruit';
            $user = 'fruit_enssop';
            $mdp = 'mdp';

            $bdd = new PDO("mysql:host=$host;dbname=$db", $user, $mdp);
            $bdd -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch(PDOException $e){
            echo "erreur a la connexion : " . $e->getMessage();
        }
        $modif = $_SESSION['nommodif'];
        $identi = $_SESSION['id'];
        $identi_type = $_SESSION['id_type'];
        $photo2 = $_SESSION['photo'];
        $nom = isset($_POST['nom']) ? $_POST['nom']:'';
        $zone = isset($_POST['zone']) ? $_POST['zone']:'';
        $type =  isset($_POST['type']) ? $_POST['type']:'';
        ?>
    <div id="head">
        <div id="titre">
            <h2>Beetle Juice</h2>
        
            <h1>Espace Admin</h1>
        </div>
    </div>
    <h2>Modif ingrédient <?php
            echo $modif;
?></h2>
    <form action="" method="post" enctype="multipart/form-data">
    <div id="formulaire">
        <div>
            <input type="text" name="nom" value="<?php
                echo $modif;
            ?>">
            <input type="file" name="fichier" id="filou">
        </div>
        <div>
            <textarea name="zone" id="" cols="30" rows="10" ><?php
                 $sql = $bdd->prepare("SELECT * FROM ingredient WHERE id_ingredient = :id");
                 $sql->execute(['id'=>$identi]);
                 $result = $sql->fetchALL();
                 foreach($result as $key){
                     echo $key['description_ing'];
                 }
                
            ?></textarea>
            <select name="type" id="ligne">
                <option value="1" <?php
                    if($identi_type == "1"){
                        echo 'selected';
                    }
                ?>>fruit</option>
                <option value="2" <?php
                    if($identi_type == "2"){
                        echo 'selected';
                    }
                ?> >legume</option>
                <option value="3" <?php
                    if($identi_type == "3"){
                        echo 'selected';
                    }
                ?>>epice</option>
            </select>
        </div>
    </div>
            <input type="submit" value="modifier">
    </form>
<?php
if(isset($_FILES['fichier'])){
    $fileName = 'uploads/'.microtime().$_FILES['fichier']['name'];
    move_uploaded_file($_FILES['fichier']['tmp_name'], $fileName);
}

try{
    if($nom != '' && $zone != '' && $fileName != '' && $type != ''){
$sql3 = $bdd->prepare("UPDATE ingredient SET nom = :nom, description_ing = :descriptio, photo = :photo, id_type = :id_type WHERE id_ingredient = :id");
$sql3->execute(['nom'=>$nom,'descriptio'=>$zone,'photo'=>$fileName,'id_type'=>$type,'id'=>$identi]);
header('Location: pageingredient.php');
    }
}catch(PDOException $e){
    echo "erreur a la connexion : " . $e->getMessage();
}
?>
</body>
</html>
<?php
session_start();
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>modification recette</title>
    <link rel="stylesheet" href="pageadmin.css">
</head>
<body>
<?php
        try{

            $host = 'localhost';
            $db = 'fruit';
            $user = 'fruit_enssop';
            $mdp = 'mdp';

            $bdd = new PDO("mysql:host=$host;dbname=$db", $user, $mdp);
            $bdd -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch(PDOException $e){
            echo "erreur a la connexion : " . $e->getMessage();
        }
        $id_recette2 = $_SESSION['id_recette'];
        $nom2 = $_SESSION['nom'];
        $nom = isset($_POST['nom']) ? $_POST['nom']:'';
        $zone = isset($_POST['zone']) ? $_POST['zone']:'';
        ?>
    <div id="head">
        <div id="titre">
            <h2>Beetle Juice</h2>
        
            <h1>Espace Admin</h1>
        </div>
    </div>
    <h2>Modif ingrédient <?php
            echo $nom2;
?></h2>
    <form action="" method="post" enctype="multipart/form-data">
    <div id="formulaire">
        <div>
            <input type="text" name="nom" value="<?php
                echo $nom2;
            ?>">
            <input type="file" name="fichier" id="filou">
        </div>
        <div>
            <textarea name="zone" id="" cols="30" rows="10" ><?php
                 $sql = $bdd->prepare("SELECT * FROM recette WHERE id_recette = :id");
                 $sql->execute(['id'=>$id_recette2]);
                 $result = $sql->fetchALL();
                 foreach($result as $key){
                     echo $key['description_recette'];
                 }
                
            ?></textarea>
        </div>
    </div>
            <input type="submit" value="modifier">
    </form>
<?php
if(isset($_FILES['fichier'])){
    $fileName = 'uploads/'.microtime().$_FILES['fichier']['name'];
    move_uploaded_file($_FILES['fichier']['tmp_name'], $fileName);
}

try{
    if($nom != '' && $zone != '' && $fileName != ''){
        
$sql3 = $bdd->prepare("UPDATE recette SET nom = :nom, description_recette = :descriptio, photo = :photo WHERE id_recette = :id");
$sql3->execute(['nom'=>$nom,'descriptio'=>$zone,'photo'=>$fileName,'id'=>$id_recette2]);
header('Location: pageingredient.php');
    }
}catch(PDOException $e){
    echo "erreur a la connexion : " . $e->getMessage();
}
?>
</body>
</html>
USE fruit

CREATE TABLE types (
    id_type             INT             AUTO_INCREMENT,
    label               VARCHAR(100)    NOT NULL,
    PRIMARY KEY (id_type)
);
CREATE TABLE ingredient (
    id_ingredient       INT             AUTO_INCREMENT,
    nom                 VARCHAR(100)    NOT NULL,
    description_ing     TEXT            NOT NULL,
    photo               VARCHAR(100)    NOT NULL,
    id_type             INT,
    PRIMARY KEY (id_ingredient),
    FOREIGN KEY (id_type) REFERENCES types(id_type)
);
CREATE TABLE recette (
    id_recette          INT             AUTO_INCREMENT,
    nom                 VARCHAR(255)    NOT NULL,
    description_recette TEXT  NOT NULL,
    photo               VARCHAR(100)    NOT NULL,
    PRIMARY KEY (id_recette)
);

CREATE TABLE ingredient_recette (
    id_recette          INT             NOT NULL,
    id_ingredient       INT             NOT NULL,
    nb_portion          INT             NOT NULL,
    PRIMARY KEY (id_recette,id_ingredient),
    FOREIGN KEY (id_ingredient) REFERENCES ingredient(id_ingredient),
    FOREIGN KEY (id_recette) REFERENCES recette(id_recette)

);
CREATE TABLE administrateur (
    id_admin            INT             AUTO_INCREMENT,
    identifiant         VARCHAR(100)    NOT NULL,
    mot_de_passe        VARCHAR(25)     NOT NULL,
    mail                VARCHAR(100)    NOT NULL,
    PRIMARY KEY (id_admin)
);
<?php
session_start();
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ingrédient</title>
    <link rel="stylesheet" href="pageadmin.css">
</head>
<body>

<?php
        try{

            $host = 'localhost';
            $db = 'fruit';
            $user = 'fruit_enssop';
            $mdp = 'mdp';

            $bdd = new PDO("mysql:host=$host;dbname=$db", $user, $mdp);
            $bdd -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch(PDOException $e){
            echo "erreur a la connexion : " . $e->getMessage();
        }
    ?>
    <div id="head">
        <div id="titre">
            <h2>Beetle Juice</h2>
        
            <h1>Espace Admin</h1>
        </div>
    </div>
    <div id="tableau">
        <div id="recette">
            <h3>Ingrédients</h3>
            <?php echo '<a href="?ajout=ok">'?>Ajouter</a>
            <div id="tableau_recette">
                <?php
                    $sql = $bdd->prepare("SELECT * FROM ingredient");
                    $sql->execute();
                    $result = $sql->fetchALL();
                    echo '<div id="supertableau">';
                    foreach($result as $key){
                        echo '<div>' . '<a href="?modif=oui&pseudo=' . htmlspecialchars($key['nom']) . '&id=' . htmlspecialchars($key['id_ingredient']) .'&id_type=' . htmlspecialchars($key['id_type']) .' ">' . $key['nom'] .  '</a></div>' /* . '<img src="'.$key['photo'].'" alt="">' */;
                        }
                    echo '</div>'
                ?>
            </div>
        </div>
        <div id="recette">
            <h3>Recettes</h3>
            <?php echo '<a href="?ajouter=ok">'?>Ajouter</a>
            <div id="tableau_recette">
            <?php
                    $sql = $bdd->prepare("SELECT * FROM recette");
                    $sql->execute();
                    $result = $sql->fetchALL();
                    echo '<div id="supertableau">';
                    foreach($result as $key){
                        echo '<div>' . '<a href="?modific=oui&nom=' . htmlspecialchars($key['nom']) . '&id_recette=' . htmlspecialchars($key['id_recette']) .' ">' . $key['nom'] .  '</a></div>' /* . '<img src="'.$key['photo'].'" alt="">' */;
                        }
                    echo '</div>'
                ?>
            </div>
        </div>
    </div>
<?php

$ajout = isset($_GET['ajout']) ? $_GET['ajout']:'';
if($ajout == 'ok'){
    header('Location: ajoutingredient.php');
}
$ajouter =isset($_GET['ajouter']) ? $_GET['ajouter']:'';
if($ajouter == 'ok'){
    header('Location: ajoutrecette.php');
}
$modif = isset($_GET['modif']) ? $_GET['modif']:'';
if($modif == 'oui'){
    header('Location: modifingredient.php');
}
$id = isset($_GET['id']) ? $_GET['id']:'';
if($id == 'oui'){
    header('Location: modifingredient.php');
}
$id_type = isset($_GET['id_type']) ? $_GET['id_type']:'';
if($id_type == 'oui'){
    header('Location: modifingredient.php');
}
$modific = isset($_GET['modific']) ? $_GET['modific']:'';
if($modific == 'oui'){
    header('Location: modifrecette.php');
}
$id_recette = isset($_GET['id_recette']) ? $_GET['id_recette']:'';
if($id == 'oui'){
    header('Location: modifrecette.php');
}
$nom = isset($_GET['nom']) ? $_GET['nom']:'';
if($nom == 'oui'){
    header('Location: modifrecette.php');
}
$modif = isset($_GET['pseudo']) ? $_GET['pseudo']:'';
$_SESSION['nommodif'] = $modif;

$id = isset($_GET['id']) ? $_GET['id']:'';
$_SESSION['id'] = $id;

$id_type = isset($_GET['id_type']) ? $_GET['id_type']:'';
$_SESSION['id_type'] = $id_type;

$id_recette = isset($_GET['id_recette']) ? $_GET['id_recette']:'';
$_SESSION['id_recette'] = $id_recette;

$nom = isset($_GET['nom']) ? $_GET['nom']:'';
$_SESSION['nom'] = $nom;
?>
</body>
</html>
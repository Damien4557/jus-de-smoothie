<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Page admin</title>
    <link rel="stylesheet" href="pageadmin.css">
</head>
<body>
<?php
        try{

            $host = 'localhost';
            $db = 'fruit';
            $user = 'fruit_enssop';
            $mdp = 'mdp';

            $bdd = new PDO("mysql:host=$host;dbname=$db", $user, $mdp);
            $bdd -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch(PDOException $e){
            echo "erreur a la connexion : " . $e->getMessage();
        }
$identifiant = isset($_POST['identifiant']) ? $_POST['identifiant']:'';
$mdp = isset($_POST['password']) ? $_POST['password']:'';
?>
    <div id="head">
        <div id="titre">
            <h2>Beetle Juice</h2>
        
            <h1>Espace Admin</h1>
        </div>
    </div>
        <h3>Connexion</h3>
    <div id="bloc">
        <form action="" method="post">
            <p>Identifiant</p><input type="text" name="identifiant" class="input">
            <p>Mot de passe</p><input type="password" name="password" class="input">
            <div id="bouton">
                <input type="submit" value="connexion">
            </div>       
        </form>
    </div>
<?php
            $sql = $bdd->prepare("SELECT * FROM administrateur WHERE identifiant = :identifiant AND  mot_de_passe = :mdp");
            $sql->execute(['identifiant' => $identifiant, 'mdp' => $mdp]);
            $result = $sql->fetchALL();
    
        if($identifiant != '' && $mdp != ''){
        foreach($result as $key)
            if ($key['identifiant'] == $identifiant && $key['mot_de_passe'] == $mdp){
                echo header('Location: pageingredient.php');
            }
        }else{
            echo 'remplir tout les champs';
        }
?>      
</body>
</html>


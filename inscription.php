<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Connexion</title>
    <link rel="stylesheet" href="pageadmin.css">
</head>
<body>
<?php
        try{

            $host = 'localhost';
            $db = 'fruit';
            $user = 'fruit_enssop';
            $mdp = 'mdp';

            $bdd = new PDO("mysql:host=$host;dbname=$db", $user, $mdp);
            $bdd -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch(PDOException $e){
            echo "erreur a la connexion : " . $e->getMessage();
        }
        $identifiant = isset($_POST['identifiant']) ? $_POST['identifiant']:'';
        $mdp = isset($_POST['password']) ? $_POST['password']:'';
        $mail = isset($_POST['mail']) ? $_POST['mail']:'';
    ?>
    <div id="head">
        <div id="titre">
            <h2>Beetle Juice</h2>
        
            <h1>Espace Admin</h1>
        </div>
    </div>
        <h3>Connexion</h3>
    <div id="bloc">
        <form action="" method="post">
            <p>Identifiant</p><input type="text" name="identifiant" class="input">
            <p>Mot de passe</p><input type="password" name="password" class="input">
            <p>E-mail</p><input type="email" name="mail" class="input">
            <div id="bouton">
                <input type="submit" value="connexion">
            </div>       
        </form>
    </div>
<?php
    try{
        if($identifiant != '' && $mdp != '' && $mail != ''){
            $insert = $bdd->prepare("INSERT INTO `administrateur`(`id_admin`, `identifiant`, `mot_de_passe`, `mail`) VALUES (NULL,?,?,?)");
            $insert->execute([$identifiant, $mdp, $mail]);
            header('Location: connexion.php');
    }else {
        echo 'réessayer';
    }
}catch (PDOException $e){
    echo "erreur a la connexion : " . $e->getMessage();
}
?>

</body>
</html>

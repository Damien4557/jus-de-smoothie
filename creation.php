<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="style.css">
    <link href="https://fonts.googleapis.com/css?family=Pacifico&display=swap" rel="stylesheet">
</head>
<body>
    <?php
    $server = 'localhost';
    $nom = 'damifruit';
    $password = '4557';

    try {
        $bdd = new PDO("mysql:host=$server;dbname=fruit;", $nom, $password);
        $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $x) {
        echo "Erreur : " . $x->getMessage();
    }
    $recupFruit = isset($_POST['fruit']) ? $_POST['fruit'] : '';
    $recupDescrip = isset($_POST['descrip']) ? $_POST['descrip'] : '';
    $recupRecette = isset($_POST['nomRecette']) ? $_POST['nomRecette'] : '';
    $fileName = isset($_POST['mon_fichier']) ? $_POST['mon_fichier'] : '';

    include 'nav.php'
    ?>
     <?php 

if (isset($_FILES['mon_fichier'])) {
    $fileName = 'uploads/'.microtime().$_FILES['mon_fichier']['name'];
    move_uploaded_file($_FILES['mon_fichier']['tmp_name'], $fileName);
}

    try {
        if ($recupDescrip != '' && $recupRecette != '' && $fileName!= '') {
        $req = $bdd->prepare("INSERT INTO recette (`nom`,`description_recette`,`photo`) VALUES (:nom,:description_recette,:photo)");
        $req->execute([
            'nom' => $recupRecette,
            'description_recette' => $recupDescrip,
            'photo' => $fileName,
        ]);
        header("Location:choixingr.php");
    } else {
        echo "Veuillez remplir tout les champs";
    } 
}catch (PDOException $x) {
    echo "Erreur : " . $x->getMessage();
}

?>
    <form action="" method="POST" enctype="multipart/form-data">
        <div class="containercreation">
            <div class="blocreation">
                <h3>Nom de votre recette :</h3>
                <input type="text" name="nomRecette" id="">
                <textarea name="descrip" id="" cols="30" rows="10"></textarea>
                <input type="submit">
            </div>
            <div class="blocreation">
                <h3>Photo de votre recette:</h3>
                <input type="file" name="mon_fichier">
                
            </div>
        </div>
    </form>

   

    <?php
$choix = isset($_GET['ajou']) ? $_GET['ajou']:'';
if($choix == 'ok'){
header('Location: choixingr.php');
}
$nom = isset($_GET['nom']) ? $_GET['nom']:'';
if($nom == 'ok'){
    $id = $bdd->lastInsertId();
    header('Location: choixingr.php?id='. htmlspecialchars($id));
}
/* $modif = isset($_GET['modif']) ? $_GET['modif']:'';
if($modif == 'oui'){
    header('Location: modifingredient.php');
}
$id = isset($_GET['id']) ? $_GET['id']:'';
if($id == 'oui'){
    header('Location: modifingredient.php');
}
$id_type = isset($_GET['id_type']) ? $_GET['id_type']:'';
if($id_type == 'oui'){
    header('Location: modifingredient.php');
}
$modific = isset($_GET['modific']) ? $_GET['modific']:'';
if($modific == 'oui'){
    header('Location: modifrecette.php');
}
$id_recette = isset($_GET['id_recette']) ? $_GET['id_recette']:'';
if($id == 'oui'){
    header('Location: modifrecette.php');
}

$modif = isset($_GET['pseudo']) ? $_GET['pseudo']:'';
$_SESSION['nommodif'] = $modif;

$id = isset($_GET['id']) ? $_GET['id']:'';
$_SESSION['id'] = $id;

$id_type = isset($_GET['id_type']) ? $_GET['id_type']:'';
$_SESSION['id_type'] = $id_type;

$id_recette = isset($_GET['id_recette']) ? $_GET['id_recette']:'';
$_SESSION['id_recette'] = $id_recette;


?>
</body>

</html>